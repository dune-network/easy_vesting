# Using Dune Wallet to interact with the vesting contract

## Configuring Dune Wallet for use with a Ledger

The first step is to head over to [https://wallet.dune.network](https://wallet.dune.network).

![](img/dune_wallet_init.png)

Click on Ledger in the top button.

You will need to choose a _derivation path_, this tells the device how
to derive your private keys. You need to input the path that you chose
when you provided the address for inclusion in the Dune genesis block
back in June. You most likely used the default path `44/1729'/0'/0'`
so you can leave this field as is.

Click on *Link DuneWallet*.

![](img/ledger_path.png)

You can now plug in your Ledger Nano S, unlock it and open the
**Dune** application or the **Tezos Wallet** application if you don't
have Dune installed.

If you use the Dune application you will see this screen:

![](img/dune_ledger_app.png)

If you use the Tezos Wallet application you will see this screen instead:

![](img/tezos_ledger_app.png)

Dune Wallet will show a popup asking you to verify you address on the
Ledger Nano S. On the Ledger you need to press the right button after
making sure the address corresponds to your own (if it is the wrong
address you probably used the wrong _derivation path_ or have the
wrong Ledger device).

On the Dune Ledger app, you will see this screen with your `dn1...`
address.

![](img/verify_dn1.jpg)

On the Tezos Ledger app, you will see this screen with your `tz1...`
address instead.

![](img/verify_tz1.jpg)

You need to make sure that the `tz1...` address corresponds to your
`dn1...` address. You can do so by going to [https://dunscan.io](https://dunscan.io) and
searching for your tz1 address (you only need to input the first
characters  as there is autocompletion in the search bar).

For instance if I go to
[https://dunscan.io/tz1TLcPZJ8t2QtWYUbNqXB48Fs2G8gE5Dx9n](https://dunscan.io/tz1TLcPZJ8t2QtWYUbNqXB48Fs2G8gE5Dx9n)
and click on the ꜩ symbol, I can see that the addresses
`tz1TLcPZJ8t2QtWYUbNqXB48Fs2G8gE5Dx9n` and
`dn1PqWLQNNremz3ns7oZasBkKpYWxCG8kGpT` correspond.

![](img/dunscan_tz1.png)

You will get a confirmation screen with the `dn1` address that was
imported. The next step is to choose a password to encrypt the wallet
(this can be anything you want).

If you have originated contracts, you will see a request to import KT
addresses into the wallet. Click Yes.

Make sure you are connected to the **Mainnet** by checking the top
right info. Finally, make sure that your account has the expected
amount (you can also double check on Dunscan).

![](img/dunewallet_main.png)


That's it, you have successfully configured Dune Wallet!

## Making the first deposit

Making the first deposit initializes the contract with the sender as
owner. For this you need to pass a special Michelson parameter: `Left Unit`.

You will also need the smart contract address for this (something like
`KT1TaCc4rxThFaDHdMCuQjGNEEh2nzSj8bMK`).

Go to the **Send** tab in the main window of Dune Wallet and paste the
contract address in the field **Destination Address**.

For the **Amount** choose the full amount of your dn1 (minus the
cents) and click **Show Advanced Options**.

In **Gas Limit**, put 150000, click **Custom Fee** and put 18000
(mudun).

Finally for the parameter field copy-paste this value:

```
Left Unit
```

![](img/deposit_parameters.png)

You can now click on **SEND**.

Make sure your Ledger is unlocked and that you are in the Dune (or
Tezos) application and confirm the _Are you sure?_ popup.

It will ask to confirm the transaction on the Ledger Nano S.

If you use the Tezos ledger wallet application, you will see:

- Unrecognized operation
- Sign hash : ...

![](img/ledger_tezos_1.jpg)
![](img/ledger_tezos_2.jpg)

You can confirm, but you cannot be 100% sure what you are signing.

If you use the Dune ledger application, you will see multiple screens:

- Confirm Contract Call
- Amount: (the amount of the transaction)
- Fee: 0.018
- Source: dn1...
- Destination: KT1...

![](img/ledger_dune_1.jpg)
![](img/ledger_dune_2.jpg)
![](img/ledger_dune_3.jpg)
![](img/ledger_dune_4.jpg)
![](img/ledger_dune_5.jpg)

Pay attention to the **Amount** and **Destination** screens and make
sure they match what you intended.

When you have confirmed the transaction on your Ledger, Dune Wallet
will display a popup indicated that the transaction was injected (if
everything went great).

After a while (~ 1 minute) you will see a new transaction appear in
the main screen of Dune Wallet, and your new balance will be
reflected.

![](img/transfer_done.png)

You can now click on the transaction, which will open a new page in
Dunscan. There you can make sure that everything went OK (the
transaction should not be red, otherwise one of the parameters was
incorrect).

### Extra checks

To make sure everything went as expected you should make the following
checks.

Go to Dunscan and type in the address of your vesting contract
(KT1...).

There you should see that it has the balance that you sent.

![](img/kt1_balance.png)

Now go to the _Code_ tab and clock on **Storage**. It should display
something like:

```
Pair (Some (Pair "dn1PqWLQNNremz3ns7oZasBkKpYWxCG8kGpT" 10001090000000))
     (Pair "2020-09-01T00:00:00Z" "2023-09-01T00:00:00Z")
```

![](img/kt1_storage.png)

In particular, pay attention to the `dn1...` value. This is the owner
of the vesting contract and the one who will be able to perform the
operations and receive the withdrawals. It should match your `dn1...`
address!


## Withdrawing money from the vesting contract

You must call the smart contract with the same account you use for
depositing the initial funds.

Proceed like to make a deposit excepted for the amount which you set
to **0** DUN. All available funds at that time will be transferred
back to the _owner_.
