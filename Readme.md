Easy vesting contracts for Dune Network
=======================================

This repository contains smart contracts for linearly vesting funds
over a period of time. There are both _Liquidity_ and _Michelson_
versions of the contracts for easy deployment and review.

Understanding and reviewing the contracts
-----------------------------------------

There are two versions of the smart contract. The one in the directory
[generic](./generic) is a configurable version where the deployer can
choose the vesting period. The one in the directory
[dune\_founders](./dune_founders) is specifically geared towards the
founders and developers of the Dune Network that were allocated tokens
in the genesis block. This last version has the vesting period
**September 1st, 2020** - **September 1st, 2023** which means that
funds are fully locked until September 1st, 2020 and fully unlocked
after September 1st, 2023. Between these two dates, funds are released
linearly.

The directory [dune\_founders](./dune_founders) contains three files:
- [easy\_vesting.liq](./dune_founders/easy_vesting.liq) : the
  Liquidity contract.
- [easy\_vesting.tz](./dune_founders/easy_vesting.tz) : the corresponding
  compiled Michelson contract.
- [easy\_vesting.init.tz](./dune_founders/easy_vesting.init.tz) : the 
  initial value of the storage in Michelson format.
  
The contracts were compiled with Liquidity version 1.055.

### Understanding the Liquidity contract

If one wants to extensively review the Liquidity contracts, the
language documentation and reference can be accessed at
http://www.liquidity-lang.org/edit/doc/index.html .

The contract has four entry points:

- `default`: This entry point is callable with no parameter (i.e. ()),
  and is used both for initialization, funding, and withdrawals.
  Initialization is performed the first time it is called. Funding
  is performed is the amount of the transaction is not null. Finally, 
  withdrawing happens if the amount is zero. In the last case, all
  available funds, at the time of the transaction, are transfered back
  to the owner. 
- `set_delegate`: This entry point is callable only by the owner, with a
  key_hash (dn1...) as parameter. If the parameter is registered as a 
  delegate, the call will succeed and the contract will delegated.
- `remove_delegate`: This entry point is callable only by the owner,
  with a unit parameter (i.e. ()). The contract will undelegated. 
- `change_owner`: This entry point is callable with a contract literal
  as parameter (dn1..., KT1...) by the current owner to transfer
  ownership. The vesting schedule remains unchanged.

### Reviewing the Michelson Smart Contract

Because Michelson is not readable, one has to ensure that the deployed
code corresponds to the original Liquidity Smart Contract present in
this repository.

This can be achieved in multiple ways. The most straightforward one is
to compile the Liquidity contract of this repository and compare the
resulting Michelson with the one stored on the blockchain.

The code of the contract stored in the blockchain can be accessed with
a block explorer such as Dunscan or directly through the node if one
does not want to rely on a third party tool as so:

```
> dune-client -A addr_of_node -P 80 get script code for KT1..

{ parameter
    (or :_entries
       (unit %default)
       (or (key_hash %set_delegate)
           (or (unit %remove_delegate) (contract :UnitContract %change_owner
unit)))) ;
...
}
```

One should also review the storage which can be read with Dunscan as
well or with a request to a Dune node:

```
> dune-client -A addr_of_node -P 80 get script code for KT1...

Pair None (Pair "2020-09-01T00:00:00Z" "2023-09-01T00:00:00Z")
```


Interacting with the Smart Contract
-----------------------------------

The various entry points can be called with specific Michelson
arguments if one uses a wallet which supports them (such as Metal,
Try-Liquidity, or Dune Wallet).

- `default`:
  In protocol 4, this can be called with a Michelson parameter of the 
  form `Left Unit`. In protocol 5, transactions without any parameters
  will trigger this entry point.
  
  Recommended settings :
  - `gas_limit` : 150000
  - `fee` : 0.018 DUN

- `set_delegate`: 
  In protocol 4, this can be called with a Michelson parameter of the 
  form `Right (Left "dn1...")`. 
  
  Recommended settings :
  - `gas_limit` : 100000
  - `fee` : 0.012 DUN

- `remove_delegate`:
  In protocol 4, this can be called with a Michelson parameter of the 
  form `Right (Right (Left Unit))`. 
  
  Recommended settings :
  - `gas_limit` : 100000
  - `fee` : 0.012 DUN

- `change_owner`:
  In protocol 4, this can be called with a Michelson parameter of the 
  form `Right (Right (Right "dn1..."))`.
  
  Recommended settings :
  - `gas_limit` : 100000
  - `fee` : 0.012 DUN

After each call one should ensure that the transaction is included in
a confirmed block and that is succeeded with a block explorer like
https://dunscan.io .
